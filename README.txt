Created by Zheng Chen on 2014-09-18.
Copyright (c) 2014 Zheng Chen, All rights reserved!

Zheng Chen (#20547901)
zheng.chen@uwaterloo.ca
Computer Science, Co-operation Program
University of Waterloo

Full instructions on how this module works can be found in file Instruction.pdf file.
This module basically just pull out the information in the University of Waterloo API based on the instructions prompted by the user.
Although it might be redundant to transfer simple readable data to lists within lists within list blah blah blah.
But the key point here is to, fully illsutrate how lists, strings, and numbers can be manipulated in Racket Language.
Please be noted that all files in this module are fully visible for everyone. It's your own responsibility to ensure that, those codes are used properly.

Contact me if you have any questions or you are interested in getting more little modules!

Survive Strive Thrive
Best Regards,
Zheng Chen